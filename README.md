## EmailService and IMessageService in separate class libraries example

I put together an example of how I implemented separate class libraries to acheive an interface and email service.

Files within the MVCStructure project to pay attention to:

Startup.cs
```csharp

services.AddTransient<IEmailService, SGEmailSerivce>();

```

``` 

The following files and code below are similar enough to get the idea of dependency injection of and IEmailService

Areas/Pages/Account/Register.cshtml.cs
Areas/Pages/Account/ForgotPassword.cshtml.cs
Areas/Pages/Account/Manage/Index.cshtml.cs
```

```csharp

    private readonly SignInManager<AppUser> _signInManager;
    private readonly UserManager<AppUser> _userManager;
    private readonly ILogger<RegisterModel> _logger;

    private readonly IEmailService _emailSender;    // <- private backing variable to store the injected interface

    public RegisterModel(
        UserManager<AppUser> userManager,
        SignInManager<AppUser> signInManager,
        ILogger<RegisterModel> logger,
        IEmailService emailSender)  // <- This is where I injected the IEmailService interface into the constructor
    {
        _userManager = userManager;
        _signInManager = signInManager;
        _logger = logger;
        _emailSender = emailSender; // <- This is where it saves the injection to the private backing variable (see above)
    }

```


