﻿using System;
using System.Threading.Tasks;
using IMessageService;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace EmailService
{
    public class SGEmailSerivce : IEmailService
    {
        private readonly SendGridAccountDetails _SendGridAccount;
        public SGEmailSerivce(IOptions<SendGridAccountDetails> optionsAccessor)
        {
            _SendGridAccount = optionsAccessor.Value ?? throw new ArgumentException(nameof(SendGridAccountDetails));
        }
        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            await Execute(email, subject, htmlMessage);
        }

        async Task Execute(string email, string subject, string htmlMessage)
        {
            var apiKey = _SendGridAccount.SGApiKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("info@wtprpc.com", "PRPC");
            // var subject = "Sent via SendGrid";
            var to = new EmailAddress($"{email}", "App User");
            var plainTextContent = "Hello from my WTPRPC App";
            // var htmlContent = $"<strong>Hello from my WTPRPC App</strong>";
            var htmlContent = htmlMessage;
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}
