using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MVCStructure.Areas.Identity.Data;

[assembly: HostingStartup(typeof(MVCStructure.Areas.Identity.IdentityHostingStartup))]
namespace MVCStructure.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<MVCStructureIdentityDbContext>(options =>
                    options.UseSqlite(
                        context.Configuration.GetConnectionString("MVCStructureIdentityDbContextConnection")));

                services.AddDefaultIdentity<AppUser>()
                    .AddEntityFrameworkStores<MVCStructureIdentityDbContext>();
            });
        }
    }
}