﻿using System;
using System.Threading.Tasks;

namespace IMessageService
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage);

    }
}
